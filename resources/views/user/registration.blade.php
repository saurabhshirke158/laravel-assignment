@extends('layouts.head')
 
@section('content')
<main class="my-form">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Make Payment</div>
                        <div class="card-body">
                            <form name="my-form" action="{{ url('placeorder') }}" method="post">{{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="full_name" class="col-md-4 col-form-label text-md-right">Transaction ID</label>
                                    <div class="col-md-6">
                                        <input type="text" id="full_name" class="form-control" name="full-name">
                                    </div>
                                </div>

                                {{-- <div class="form-group row">
                                    <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail ID</label>
                                    <div class="col-md-6">
                                        <input type="text" id="email_address" class="form-control" name="email_address">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone_number" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                                    <div class="col-md-6">
                                        <input type="text" id="phone" class="form-control" name="phone" maxlength="10" minlength="10">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="user_name" class="col-md-4 col-form-label text-md-right">City</label>
                                    <div class="col-md-6">
                                        <input type="text" id="city" class="form-control" name="city">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="permanent_address" class="col-md-4 col-form-label text-md-right">State</label>
                                    <div class="col-md-6">
                                        <input type="text" id="state" class="form-control" name="state">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="present_address" class="col-md-4 col-form-label text-md-right">Address</label>
                                    <div class="col-md-6">
                                        <input type="text" id="address" class="form-control" onkeyup="getAddress();">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="permanent_address" class="col-md-4 col-form-label text-md-right">Zip Code</label>
                                    <div class="col-md-6">
                                        <input type="text" id="zipcode" class="form-control" name="zipcode">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="permanent_address" class="col-md-4 col-form-label text-md-right">Country</label>
                                    <div class="col-md-6">
                                        <input type="text" id="country" class="form-control" name="country">
                                    </div>
                                </div> --}}

                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" id="save_user" class="btn btn-primary">
                                        Place Order
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
        </div>
    </div>

</main>
<script>
    
    function getAddress() {
        var address = $("#address").val();
        var city = $("#city").val();
        var state = $("#state").val();

        var form = new FormData();

        form.append('street', address);
        form.append('city', city);
        form.append('state',state);
        form.append('_token', '{{csrf_token()}}');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            type: "POST",
            url: '{{ url('getAddress') }}',
            data: form,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
            }

        });
    }

    function validform() {

        var name = $("#full_name").val();
        var email_address = $("#email_address").val();
        var phone = $("#phone").val();
        var address = $("#address").val();
        var city = $("#city").val();
        var state = $("#state").val();
        var zipcode = $("#zipcode").val();
        var country = $("#country").val();

        if (name==null || name=="")
        {
            alert("Please Enter Your Full Name");
            return 0;
        }else if (email_address==null || email_address=="")
        {
            alert("Please Enter Your Email Address");
            return 0;
        }else if (phone==null || phone=="" || phone.length < 10 || phone.length > 10)
        {
            alert("Please Enter Your Valid Phone Number");
            return 0;
        }else if (address==null || address=="")
        {
            alert("Please Enter Your Permanent Address");
            return 0;
        }else if (city==null || city=="")
        {
            alert("Please Enter Your City");
            return 0;
        }else if (state==null || state=="")
        {
            alert("Please Enter Your State");
            return 0;
        }else if (zipcode==null || zipcode=="")
        {
            alert("Please Enter Your Country");
            return 0;
        }else if (country==null || country=="")
        {
            alert("Please Enter Your Zip Code");
            return 0;
        } else {
            return 1;
        }

    }

    function save() {
        $validate = 1;

        if($validate == 1) {
            console.log('im here')
            var name = $("#full_name").val();

            var form = new FormData();

            form.append('transaction_no', name);

            if (confirm('Are you sure you want to place order?')) {
                $("#save_user").prop('disabled',true);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    type: "POST",
                    url: '{{ url('placeorder') }}',
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $("#full_name").val('');
                        $("#save_user").prop('disabled',false);
                        alert(data.msg);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("#save_user").prop('disabled',false);
                        alert('Error adding new user');
                    }
                });
            }
        }
    }
</script>
@endsection