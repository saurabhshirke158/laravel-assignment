Migration file is added to create user table

Run "php artisan migrate" command to create tables

"php artisan serve" to run the project

Points included in project 
    - Create UI to register user
    - Validation of user inputs 
    - Save user details in table
    - Send email once data is stored
    - Have created getAddress API to find zipcode and country, but address autocomplete API is in paid account and not available in free. That's why it's not integrated in UI.