<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Mail;
use Illuminate\Support\Facades\Log;
use Softon\Indipay\Facades\Indipay;  

class UserController extends Controller
{
    public function __construct()
    {
        $this->users = new Users();
    }

    public function registerUser(Request $request){


        if(isset($request->name) && isset($request->email_address) && isset($request->phone) && isset($request->address) && isset($request->city) && isset($request->state) && isset($request->zipcode) && isset($request->country)) {
            $savedata = array();

            $savedata['name'] = $request->name;
            $savedata['email'] = $request->email_address;
            $savedata['phone'] = $request->phone;
            $savedata['address'] = $request->address;
            $savedata['city'] = $request->city;
            $savedata['state'] = $request->state;
            $savedata['zipcode'] = $request->zipcode;
            $savedata['country'] = $request->country;
            $savedata['status'] = 'active';
            $savedata['remember_token'] = Str::random(20);
            $savedata['created_at'] = Carbon::now();
            $savedata['updated_at'] = Carbon::now();

            $registerUser = $this->users;

            $getres = $registerUser->saveUser($savedata);

            if ($getres) {
                $to_name = $savedata['name'];
                $to_email = $savedata['email'];

                $data = array("name"=>$to_name, "body" => "Your registration is successfully done!");

                try {
                    Mail::send('emails.register_mail', $data, function($message) use ($to_name, $to_email) {
                        $message->to($to_email, $to_name)
                        ->subject("Registration Successful");
                        $message->from('saurabhshirke158@gmail.com','Saurabh');
                    });
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                }
                
                return response()->json(['msg'=>'User Added Successfully and email sent.', 'user_id' => $getres], 200);
            } else {
                return response()->json(['msg'=>'Error adding user'], 400);
            }

        }
        else {
            return "Mandatory fields error";
        }
    }

    public function placeorder(Request $request){
        $transaction_no = $request->transaction_no;
        $parameters = [
            'tid' => $transaction_no,
            'order_id' => $transaction_no,
            'amount' => 1200,
            'firstname' => 'Jon',
            'lastname' => 'Doe',
            'email' => 'jon@doe.com',
            'phone' => '1234567890',
            'productinfo' => '123prod',
            'service_provider' => '',
            'zipcode' => '400011',
            'city' => 'Mumbai',
            'state' => 'Maharashtra',
            'country' => 'India',
            'address1' => 'abc',
            'address2' => 'xyz',
            'surl' => url('https://testing.achintyalabs.com/payumoney/success.php'),
            'furl' => url('https://testing.achintyalabs.com/payumoney/failure.php'),
          ];
          
          $order = Indipay::prepare($parameters);
          return Indipay::process($order);
    }
}