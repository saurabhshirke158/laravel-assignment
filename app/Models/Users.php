<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Users extends Model
{
    protected $table = 'users';
    public $timestamps = true;


    public function saveUser($savedata) {

        $saveDetails = DB::table('users')->insertGetId($savedata);

        if($saveDetails>0) {
            return $saveDetails;
        }
        else{
            return "Error Saving Notification";
        }

    }

}