<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('user.registration');
});

Route::post('/registerUser','App\Http\Controllers\UserController@registerUser');
Route::POST('/getAddress','App\Http\Controllers\AddressController@getAddress');
Route::POST('/placeorder','App\Http\Controllers\UserController@placeorder');
